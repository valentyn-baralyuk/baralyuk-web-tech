var express = require('express');
var router = express.Router();
var controllers = require('../controllers');

router.get('/', controllers.indexController);

router.get('/students/:id/destroy', controllers.handleRemoveStudent);
router.get('/students/:stud_id/subjects/:subj_id/destroy', controllers.handleRemoveSubject);
router.get('/students/:stud_id/subjects/:subj_id/tasks/:task_id/destroy', controllers.handleRemoveTask);
router.post('/students/create', controllers.handleCreateStudent);
router.get('/students/:id', controllers.studentController);
router.get('/students/:stud_id/subjects/:subj_id', controllers.subjectController)
router.post('/students/:stud_id/subjects/create', controllers.handleCreateSubject);
router.post('/students/:stud_id/subjects/:subj_id/tasks/create', controllers.handleCreateTask);

module.exports.router = router;