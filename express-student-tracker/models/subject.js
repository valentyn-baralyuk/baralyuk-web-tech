'use strict';
module.exports = (sequelize, DataTypes) => {
  const Subject = sequelize.define('Subject', {
    name: DataTypes.STRING,
    score: DataTypes.INTEGER
  }, {});
  Subject.associate = function(models) {
    Subject.belongsTo(models.Student, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
    Subject.hasMany(models.Task);
  };
  return Subject;
};