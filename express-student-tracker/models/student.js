'use strict';
module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    year: DataTypes.INTEGER,
    group: DataTypes.STRING
  }, {});
  Student.associate = function(models) {
    Student.hasMany(models.Subject);
  };
  return Student;
};