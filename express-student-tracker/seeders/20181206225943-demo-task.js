'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Tasks', [
      {
        id: 1,
        title: 'Lab 1',
        mark: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
        SubjectId: 1
      },
      {
        id: 2,
        title: 'Lab 1',
        mark: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
        SubjectId: 2
      },
      {
        id: 3,
        title: 'Test 1',
        mark: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
        SubjectId: 3
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Tasks', null, {});
  }
};
