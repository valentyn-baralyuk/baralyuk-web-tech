'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Students', [
      {
        id: 1,
        firstName: 'Valentyn',
        lastName: 'Baralyuk',
        year: 3,
        group: 'KNIT-16-1',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,        
        firstName: 'Roman',
        lastName: 'Zolotayko',
        year: 3,
        group: 'KNIT-16-1',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        firstName: 'Artem',
        lastName: 'Tkach',
        year: 3,
        group: 'KNIT-16-1',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Students', null, {});
  }
};
