'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Subjects', [
      {
        id: 1,
        name: 'Web-Tech',
        score: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
        StudentId: 1
      },
      {
        id: 2,
        name: 'Systemic Analysis',
        score: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
        StudentId: 2
      },
      {
        id: 3,
        name: 'Math',
        score: 0,
        createdAt: new Date(),
        updatedAt: new Date(),
        StudentId: 3
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Subjects', null, {});
  }
};
