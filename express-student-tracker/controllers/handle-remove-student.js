var models = require('../models');

module.exports.handleRemoveStudent = (req, res) => {
  models.Student.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.redirect('/');
  });
}