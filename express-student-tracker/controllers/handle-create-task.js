var models = require('../models');

module.exports.handleCreateTask = (req, res) => {
  models.Task.create({
    title: req.body.title,
    mark: req.body.mark,
    SubjectId: req.params.subj_id
  }).then(() => {
      res.redirect('/students/' + req.params.stud_id + '/subjects/' + req.params.subj_id);
  });
}