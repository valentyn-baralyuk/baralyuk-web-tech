var models = require('../models');

module.exports.handleCreateSubject = (req, res) => {
  models.Subject.create({
    name: req.body.name,
    score: 0,
    StudentId: req.params.stud_id
  }).then(() => {
    res.redirect('/students/' + req.params.stud_id);
  });
}