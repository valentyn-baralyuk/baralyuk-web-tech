var models = require('../models');

module.exports.handleCreateStudent = (req, res) => {
  models.Student.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    year: req.body.year,
    group: req.body.group
  }).then(() => {
    res.redirect('/');
  });
}