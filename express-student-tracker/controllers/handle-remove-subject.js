var models = require('../models');

module.exports.handleRemoveSubject = (req, res) => {
  models.Subject.destroy({
    where: {
      id: req.params.subj_id
    }
  }).then(() => {
    res.redirect('/students/' + req.params.stud_id);
  });
}