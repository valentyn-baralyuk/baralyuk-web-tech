var {indexController} = require('./index-controller');
var {subjectController} = require('./subject-controller');
var {handleCreateStudent} = require('./handle-create-student');
var {handleCreateTask} = require('./handle-create-task');
var {handleCreateSubject} = require('./handle-create-subject');
var {handleRemoveSubject} = require('./handle-remove-subject');
var {handleRemoveStudent} = require('./handle-remove-student');
var {handleRemoveTask} = require('./handle-remove-task');
var {studentController} = require('./student-controller');


module.exports.indexController = indexController;
module.exports.subjectController = subjectController;
module.exports.handleCreateStudent= handleCreateStudent;
module.exports.handleCreateTask = handleCreateTask;
module.exports.handleCreateSubject = handleCreateSubject;
module.exports.handleRemoveSubject = handleRemoveSubject;
module.exports.handleRemoveStudent = handleRemoveStudent;
module.exports.handleRemoveTask = handleRemoveTask;
module.exports.studentController = studentController;