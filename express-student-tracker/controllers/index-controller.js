var models = require('../models');

module.exports.indexController = (req, res) => {
  models.Student.findAll({
    include: [ models.Subject ],
    order: [
      ['id', 'ASC'],
    ],
  }).then((students) => {
    res.render('home', {
      title: 'Stdunent list',
      students: students
    });
  });
}
