var models = require('../models');

module.exports.subjectController = (req, res) => {
  models.Subject.findByPk(req.params.subj_id, { include: [{ all: true, nested: true }]})
  .then((subject) => {
    res.render('subj-profile', {
      subject: subject
    });
  });
}