var models = require('../models');

module.exports.handleRemoveTask = (req, res) => {
  models.Task.destroy({
    where: {
      id: req.params.task_id
    }
  }).then(() => {
    res.redirect('/students/' + req.params.stud_id + '/subjects/' + req.params.subj_id);
  });
}