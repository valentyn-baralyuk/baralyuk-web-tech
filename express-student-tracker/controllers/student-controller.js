var models = require('../models');

module.exports.studentController = (req, res) => {
  models.Student.findByPk(req.params.id, { include: [{ all: true, nested: true }]})
  .then((student) => {
    res.render('stud-profile', {
      student: student
    });
  });
}