var { view } = require('../view-engine/view-engine');
var { Terrorist } = require('../models/terrorist');

let terroristListController = (req, res) => {
    Terrorist.getAll()
        .then(terrorists => {
            view(req, res, 'terroristList', terrorists);
        });
}

exports.terroristListController = terroristListController;