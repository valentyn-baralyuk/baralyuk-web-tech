// 1. За допомогою конструкції if-else перевірити число на парність і вивести на екран.

var num = 88;
if (num % 2 === 0) {
    console.log(num + " is even")
    } else {
    console.log(num + " is odd")
}

/* 2. Створити функцію, яка в якості аргументу приймає іншу функцію. Функція вищого 
рівня повинна містити в собі змінну строкового типу. При виклику переданої функції 
вона повинна передати строку в неї. Callback функція, повинна перевірити довжину, 
переданої в неї строки(якщо довжина більше 10 вивести її). */

function highOrderFunc(callback){
    var str = "this is a string";
    callback(str);
}

highOrderFunc(function(str) {
    str.length > 10 ? console.log(str) : console.log('string length is less than 10')
})

/* 3. Створити клас Animal з полем name. Створити Дочірні класи Dog, Cat з полем age і 
методом sayName(метод виводить ім’я тварини) */

function Animal(name){
    this.name = name;
}
function Dog(name, age){
    Animal.call(this, name);
    this.age = age,
    this.sayName = function() {
        console.log(`This dog's name is ${this.name}`) 
    };
}
function Cat(name, age){
    Animal.call(this, name);
    this.age = age,
    this.sayName = function() {
        console.log(`This cat's name is ${this.name}`) 
    };
}

Dog.prototype = Object.create(Animal.prototype);
Dog.prototype.constructor = Dog;
Cat.prototype = Object.create(Animal.prototype);
Cat.prototype.constructor = Cat;

var cat = new Cat('Whiskers', 8);
cat.sayName();
var dog = new Dog('Doom', 9);
dog.sayName();