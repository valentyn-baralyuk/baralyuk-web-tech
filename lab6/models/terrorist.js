'use strict';
module.exports = (sequelize, DataTypes) => {
  const Terrorist = sequelize.define('Terrorist', {
    name: DataTypes.STRING,
    health: DataTypes.INTEGER,
    damage: DataTypes.INTEGER,
    isGoodTerrorist: DataTypes.BOOLEAN
  }, {});
  Terrorist.associate = function(models) {
    // associations can be defined here
  };
  return Terrorist;
};