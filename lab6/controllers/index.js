var {indexController} = require('./index-controller');
var {terroristListController} = require('./terrorist-list-controller');
var {addTerrorist} = require('./add-terrorist');
var {handleAddController} = require('./handle-add-controller');
var {handleRemoveController} = require('./handle-remove-controller')

module.exports.indexController = indexController;
module.exports.terroristListController = terroristListController;
module.exports.addTerrorist= addTerrorist;
module.exports.handleAddController = handleAddController;
module.exports.handleRemoveController = handleRemoveController;

