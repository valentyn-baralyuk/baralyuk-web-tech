var {Terrorist} = require('../models');

module.exports.terroristListController = (req, res) => {
   Terrorist.findAll()
   .then(data => {
       res.render('list', {list: data});
   });
   
}